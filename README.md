# Berlatih-html

## Target
* Menguasai dasar-dasar HTML
* Membangun layout website sederhana

## Petunjuk Pengerjaan
1. Buat File HTML index
Buatlah sebuah file index.html yang akan menampilkan halaman seperti berikut: 
![Index HTML](index.png)

2. Buat halaman form
Buatlah file baru bernama form.html yang berisi form seperti berikut :
![Form HTML](form.png)

3. Buat halaman selamat datang
Setelah user berhasil mendaftar, berikan halaman welcome.html yang memberikan ucapan selamat datang seperti berikut: 
![Welcome HTML](welcome.png)

4. Hasil Akhir
Setelah semua file html yang kita perlukan siap, maka website kita sudah berjalan jika kita mulai dari index.html
![Simulasi](simulatedhtml.gif)